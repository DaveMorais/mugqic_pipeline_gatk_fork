#!/usr/bin/perl -w

use strict;
use Data::Dumper;

my $sampleFile      = $ARGV[0];
my $numberOfJobsAcc = $ARGV[1];    # number of jobs that will be accumulated
my $parallelJobs    = $ARGV[2];    # numer of jobs running in parallel
my $stepName        = $ARGV[3];    # name of the step that will be accumulated (as it is at the begin of the line job submission)

if (scalar(@ARGV)!=4 ){
    
    print "usage:\n";
    print "\nperl jobAcummulate.pl <job file *.sh> <number-of-jobs-to-accumulate> <Number-of-jobs_running-in-parallel> <step-name as in sh file (i.e. MPILEUP_JOB_ID)>\n";
    exit;
}


my @lines;
my $replacement = $stepName . "_ACC";
my $bqsub       = 'bqsub_accumulator';
my $stepCounter = 0;



open( IN, $sampleFile ) || die "could not open $sampleFile, $!\n";

while (<IN>) {
    push( @lines, $_ );
}

for ( my $l = 0 ; $l <= $#lines ; $l++ ) {

    if ( $lines[$l] =~ /^#!\/bin\/bash/ ) {
        print $lines[$l];
        print "rm -rf ~/.bqmono\n";
        print "export BQACCJOBS=9999999999\n";
        print "export BQRUNJOBS=$parallelJobs\n";
        next;
    }
    if ( $lines[$l] =~ /^$stepName=\$\(echo/ ) {

        $stepCounter++;
        $lines[$l] =~ s/$stepName/$replacement/;
        $lines[$l] =~ s/qsub/$bqsub/;
        print $lines[$l];

        if ( $stepCounter == $numberOfJobsAcc ) {
            print "\n" . $stepName . '=$(bqsub_accumulator --submit)', "\n";
            print 'echo "$MPILEUP_JOB_ID   $JOB_NAME       $JOB_DEPENDENCIES       $JOB_OUTPUT123" >> $JOB_LIST', "\n";
            $stepCounter = 0;
            $l++;    # do not print echo line
            next;
        }

        if ( ( $stepCounter < $numberOfJobsAcc ) && ( $lines[ $l + 5 ] !~ /^\#\s+$stepName/ ) ) {

            print "\n" . $stepName . '=$(bqsub_accumulator --submit)', "\n";
            print 'echo "$MPILEUP_JOB_ID   $JOB_NAME       $JOB_DEPENDENCIES       $JOB_OUTPUT" >> $JOB_LIST', "\n";
            $stepCounter = 0;
            $l++;    # do not print echo line
            next;
        }
        else {
            print "\n\n";
            $l += 3;    # do not print echo line and job id accumulation line
        }

    }

    else {
        print $lines[$l];
    }
}
